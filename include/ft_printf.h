/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 13:01:36 by npatton           #+#    #+#             */
/*   Updated: 2018/08/09 19:07:23 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <stdarg.h>
# include <locale.h>
# include <stdint.h>

/*
**				Structs
*/

typedef struct	s_env
{
	int			fd;
	const char	*fmt;
	int			i;
	char		*buf;
	char		*arg;
	int			spac;
	int			plus;
	int			left;
	int			hash;
	int			wide;
	int			prec;
	int			zero;
	char		type;
	int			leng;
	int			neg;
	int			l;
	int			h;
	int			j;
	int			z;
}				t_env;

/*
**				Printf Functions
*/

int				ft_printf(const char *format, ...);
int				ft_dprintf(int fd, const char *format, ...);

/*
**				Parsers
*/

void			parse_fmt(t_env *e, va_list arg);
void			parse_flag(t_env *e);
void			parse_wide(t_env *e, va_list arg);
void			parse_prec(t_env *e, va_list arg);
void			parse_type(t_env *e, va_list arg);

/*
**				Converters
*/

void			convert_p(t_env *e);
void			convert_o(t_env *e, va_list arg);
void			convert_m(t_env *e, va_list arg);
void			convert_u(t_env *e, va_list arg);
void			convert_i(t_env *e, va_list arg);
void			convert_c(t_env *e, va_list arg);
void			convert_s(t_env *e, va_list arg);
void			convert_x(t_env *e, va_list arg);

/*
**				Printers
*/

void			print_flag(char *str, t_env *e);
void			print_zx(t_env *e);
void			print_numprefix(t_env *e);
void			print_wide(t_env *e);
void			print_prec(char *str, int p, t_env *e);

/*
**				Helpers
*/

void			add_flag(t_env *e);
void			add_leng(t_env *e);
int				get_wide(t_env *e);
int				fmt_nums(t_env *e, va_list arg);

/*
**				Environment
*/

t_env			*env_alloc(int fd);
void			env_init(t_env *e);

/*
**				Buffer
*/

void			buf_chr(char c, t_env *e);
void			buf_str(char *str, t_env *e);
void			buf_wchr(wchar_t wc, t_env *e);
void			buf_wstr(wchar_t *str, t_env *e);

#endif
