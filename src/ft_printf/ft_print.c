/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 20:18:24 by npatton           #+#    #+#             */
/*   Updated: 2018/11/02 14:40:19 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		print_flag(char *str, t_env *e)
{
	e->leng = ft_strlen(str);
	if (e->type == 'i' && !e->neg && e->zero && e->plus)
		buf_chr('+', e);
	if ((ft_strchr("oxX", e->type) && e->hash && e->zero) || e->type == 'p')
		print_zx(e);
	if (!e->left)
		print_wide(e);
	if (e->type == 'i' && !e->neg && !e->zero && e->plus)
		buf_chr('+', e);
	else if (e->type == 'i' && !e->neg && e->spac)
		buf_chr(' ', e);
	if (ft_strchr("poxX", e->type) && e->hash && !e->zero)
		print_zx(e);
	print_prec(str, e->prec, e);
	if (e->left)
		print_wide(e);
}

void		print_zx(t_env *e)
{
	if (e->type == 'p')
		buf_str("0x", e);
	if (e->type == 'x')
		buf_str("0x", e);
	if (e->type == 'X')
		buf_str("0X", e);
	if (e->type == 'o')
		buf_chr('0', e);
}

void		print_numprefix(t_env *e)
{
	int			len;

	if (e->prec > e->leng)
	{
		len = e->prec - e->leng;
		if (e->neg && !e->left)
			buf_chr('-', e);
		while (len--)
			buf_chr('0', e);
	}
}

void		print_wide(t_env *e)
{
	int		len;

	if ((e->wide > e->leng && !e->prec) || (e->wide > e->prec && e->prec) ||
		(ft_strchr("sScC", e->type) && e->prec >= e->leng && e->prec >= e->wide))
	{
		len = get_wide(e);
		if (e->zero && !e->left)
		{
			if (e->neg)
				buf_chr('-', e);
			while (len--)
				buf_chr('0', e);
			return ;
		}
		while (len--)
			buf_chr(' ', e);
	}
}

void		print_prec(char * str, int p, t_env *e)
{
	if (e->zero && e->neg && !e->left && (e->wide > e->leng || p > e->leng))
		str++;
	if (e->prec)
	{
		if (ft_strchr("sc", e->type))
		{
			while (p-- && *str)
				buf_chr(*str++, e);
		}
		else if (ft_strchr("iopuxX", e->type))
		{
			print_numprefix(e);
			buf_str(str, e);
		}
	}
	else
		buf_str(str, e);
}
