/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_helpers.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 20:18:24 by npatton           #+#    #+#             */
/*   Updated: 2018/10/09 15:43:49 by hackathon        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"


void		add_flag(t_env *e)
{
	if (!ft_strchr(" -+0#", e->fmt[e->i]))
		return ;
	if (e->fmt[e->i] == '#')
		e->hash = 1;
	if (e->fmt[e->i] == '-')
		e->left = 1;
	if (e->fmt[e->i] == '+')
		e->plus = 1;
	if (e->fmt[e->i] == '0')
		e->zero = 1;
	if (e->fmt[e->i] == ' ')
		e->spac = 1;
	e->i++;
}

void		add_leng(t_env *e)
{
	if (!ft_strchr("lhjz", e->fmt[e->i]))
		return ;
	if (e->fmt[e->i] == 'l' && e->l < 2 && !e->h && !e->j && !e->z)
		e->l++;
	if (e->fmt[e->i] == 'h' && e->h < 2 && !e->l && !e->j && !e->z)
		e->h++;
	if (e->fmt[e->i] == 'j' && !e->l && !e->h && !e->z)
		e->j++;
	if (e->fmt[e->i] == 'z' && !e->l && !e->h && !e->j)
		e->z++;
	e->i++;
}

int			fmt_nums(t_env *e, va_list arg)
{
	int		n;

	n = 0;
	if (e->fmt[e->i] == '*')
	{
		n = va_arg(arg, int);
		e->i++;
	}
	else
	{
		while (ft_isdigit(e->fmt[e->i]))
			n = (n * 10) + (e->fmt[e->i++] - '0');
	}
	return (n);
}

int			get_wide(t_env *e)
{
	int		len;

	len = 0;
	if (e->prec)
	{
		if (ft_strchr("sScC", e->type) && e->prec >= e->leng)
			len = e->wide - e->leng;
		else if (e->prec >= e->wide)
			len = 0;
		else if (ft_strchr("ipoxX", e->type) && e->prec >= e->leng)
			len = e->wide - e->prec;
		else if (ft_strchr("ipoxX", e->type) && e->leng >= e->prec)
			len = e->wide - e->leng;
		else if (e->leng >= e->prec)
			len = e->wide - e->prec;
	}
	else
		len = e->wide - e->leng;
	if ((e->type == 'i' && !e->neg && (e->plus || e->spac)) ||
		(e->type == 'o' && e->hash))
		len--;
	if (ft_strchr("xX", e->type) && e->hash)
		len -= 2;
	return (len);
}
