/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/13 19:13:02 by npatton           #+#    #+#             */
/*   Updated: 2018/10/13 19:13:07 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		parse_fmt(t_env *e, va_list arg)
{
	if (e->fmt[e->i] != '%')
		return ;
	e->i++;
	if (!e->fmt[e->i])
		return ;
	parse_flag(e);
	parse_wide(e, arg);
	parse_prec(e, arg);
	parse_flag(e);
	parse_type(e, arg);
	env_init(e);
}

void		parse_flag(t_env *e)
{
	while (ft_strchr(" -+0#lhjz", e->fmt[e->i]))
	{
		add_flag(e);
		add_leng(e);
		if (!e->fmt[e->i])
			break;
	}
}

void		parse_wide(t_env *e, va_list arg)
{
		e->wide = fmt_nums(e, arg);
}

void		parse_prec(t_env *e, va_list arg)
{
	if (e->fmt[e->i] == '.')
	{
		e->i++;
		e->prec = fmt_nums(e, arg);
	}
}

void		parse_type(t_env *e, va_list arg)
{
	if (!ft_strchr("%cCidDoOpsSuUxX", e->fmt[e->i]))
		return ;
	if (e->fmt[e->i] == '%')
		convert_p(e);
	else if (ft_strchr("oO", e->fmt[e->i]))
		convert_o(e, arg);
	else if (ft_strchr("p", e->fmt[e->i]))
		convert_m(e, arg);
	else if (ft_strchr("uU", e->fmt[e->i]))
		convert_u(e, arg);
	else if (ft_strchr("idD", e->fmt[e->i]))
		convert_i(e, arg);
	else if (ft_strchr("cC", e->fmt[e->i]))
		convert_c(e, arg);
	else if (ft_strchr("sS", e->fmt[e->i]))
		convert_s(e, arg);
	else if (ft_strchr("xX", e->fmt[e->i]))
		convert_x(e, arg);
	e->i++;
}
