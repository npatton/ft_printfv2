/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/13 19:13:02 by npatton           #+#    #+#             */
/*   Updated: 2018/10/13 19:13:07 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_env		*env_alloc(int fd)
{
	t_env	*e;

	if (!(e = ft_memalloc(sizeof(t_env))))
		exit (2);
	e->buf = ft_memalloc(sizeof(char) * 1);
	e->i = 0;
	e->fd = fd;
	return (e);
}

void		env_init(t_env *e)
{
	e->spac = 0;
	e->plus = 0;
	e->left = 0;
	e->hash = 0;
	e->wide = 0;
	e->prec = 0;
	e->zero = 0;
	e->spac = 0;
	e->type = 0;
	e->leng = 0;
	e->neg = 0;
	e->l = 0;
	e->h = 0;
	e->j = 0;
	e->z = 0;
}
