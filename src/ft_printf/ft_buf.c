/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_buf.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/13 19:13:02 by npatton           #+#    #+#             */
/*   Updated: 2018/11/02 14:38:32 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		buf_str(char *str, t_env *e)
{
	char	*tmp;

	tmp = e->buf;
	e->buf = ft_strjoin(e->buf, str);
	ft_memdel((void*)&tmp);
}

void		buf_chr(char c, t_env *e)
{
	char	*str;

	if (!(str = ft_memalloc(sizeof(char) * 2)))
		exit(2);
	str[0] = c;
	str[1] = '\0';
	buf_str(str, e);
	ft_memdel((void*)&str);
}

void		buf_wstr(wchar_t *str, t_env *e)
{
	while (*str)
		buf_chr(*(str++), e);
}

void		buf_wchr(wchar_t wc, t_env *e)
{
	if (wc <= 127)
		buf_chr(wc, e);
	else if (wc <= 2047)
	{
		buf_chr(((wc >> 6) + 0xC0), e);
		buf_chr(((wc & 0x3F) + 0x80), e);
	}
	else if (wc <= 65535)
	{
		buf_chr(((wc >> 12) + 0xE0), e);
		buf_chr((((wc >> 6) & 0x3F) + 0x80), e);
		buf_chr(((wc & 0x3F) + 0x80), e);
	}
	else if (wc <= 1114111)
	{
		buf_chr(((wc >> 18) + 0xF0), e);
		buf_chr((((wc >> 12) & 0x3F) + 0x80), e);
		buf_chr((((wc >> 6) & 0x3F) + 0x80), e);
		buf_chr(((wc & 0x3F) + 0x80), e);
	}
}
