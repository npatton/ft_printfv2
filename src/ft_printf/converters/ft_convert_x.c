/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_x.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		convert_x(t_env *e, va_list arg)
{
	char	*tmp;

	e->type = e->fmt[e->i];
	if (e->l == 1)
		tmp = ft_ltoabase(va_arg(arg, unsigned long), 16);
	else if (e->l == 2)
		tmp = ft_ltoabase(va_arg(arg, unsigned long long), 16);
	else if (e->h == 1)
		tmp = ft_ltoabase((unsigned short)va_arg(arg, unsigned int), 16);
	else if (e->h == 2)
		tmp = ft_ltoabase((unsigned char)va_arg(arg, unsigned int), 16);
	else if (e->j == 1)
		tmp = ft_ltoabase(va_arg(arg, uintmax_t), 16);
	else if (e->z == 1)
		tmp = ft_ltoabase(va_arg(arg, size_t), 16);
	else
		tmp = ft_ltoabase(va_arg(arg, unsigned int), 16);
	print_flag(tmp, e);
	ft_memdel((void*)&tmp);
}
