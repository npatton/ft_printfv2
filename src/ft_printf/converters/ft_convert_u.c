/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		convert_u(t_env *e, va_list arg)
{
	char	*tmp;

	e->type = 'u';
	if (e->l == 1)
		tmp = ft_ltoa(va_arg(arg, unsigned long));
	else if (e->l == 2)
		tmp = ft_ltoa(va_arg(arg, unsigned long long));
	else if (e->h == 1)
		tmp = ft_ltoa((unsigned short)va_arg(arg, unsigned int));
	else if (e->h == 2)
		tmp = ft_ltoa((unsigned char)va_arg(arg, unsigned int));
	else if (e->j == 1)
		tmp = ft_ltoa(va_arg(arg, uintmax_t));
	else if (e->z == 1)
		tmp = ft_ltoa(va_arg(arg, size_t));
	else
		tmp = ft_itoa(va_arg(arg, unsigned int));
	print_flag(tmp, e);
	ft_memdel((void*)&tmp);
}
