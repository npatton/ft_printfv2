/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_i.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/10/09 15:43:46 by hackathon        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		convert_i(t_env *e, va_list arg)
{
	char	*tmp;

	e->type = 'i';
	if (e->l == 1)
		tmp = ft_ltoa(va_arg(arg, long));
	else if (e->l == 2)
		tmp = ft_ltoa(va_arg(arg, long long));
	else if (e->h == 1)
		tmp = ft_ltoa((short)va_arg(arg, int));
	else if (e->h == 2)
		tmp = ft_ltoa((char)va_arg(arg, int));
	else if (e->j == 1)
		tmp = ft_ltoa(va_arg(arg, intmax_t));
	else if (e->z == 1)
		tmp = ft_ltoa(va_arg(arg, ssize_t));
	else
		tmp = ft_ltoa(va_arg(arg, int));
	if (*tmp == '-')
		e->neg = 1;
	print_flag(tmp, e);
	ft_memdel((void*)&tmp);
}
