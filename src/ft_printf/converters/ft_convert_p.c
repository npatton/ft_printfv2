/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_p.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		convert_p(t_env *e)
{
	char	*tmp;

	e->type = 's';
	tmp = (char *)malloc(sizeof(char) * 2);
	tmp[0] = '%';
	tmp[1] = '\0';
	print_flag(tmp, e);
	ft_memdel((void*)&tmp);
}
