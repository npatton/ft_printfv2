/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_c.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/11/02 14:37:26 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		convert_c(t_env *e, va_list arg)
{
	char	*tmp;

	e->type = 's';
	tmp = (char *)malloc(sizeof(char) * 2);
	tmp[0] = va_arg(arg, int);
	tmp[1] = '\0';
	print_flag(tmp, e);
	ft_memdel((void*)&tmp);
}
