/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_o.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		convert_o(t_env *e, va_list arg)
{
	char	*tmp;

	e->type = 'o';
	if (e->l == 1)
		tmp = ft_ltoabase(va_arg(arg, unsigned long), 8);
	else if (e->l == 2)
		tmp = ft_ltoabase(va_arg(arg, unsigned long long), 8);
	else if (e->h == 1)
		tmp = ft_ltoabase((unsigned short)va_arg(arg, unsigned int), 8);
	else if (e->h == 2)
		tmp = ft_ltoabase((unsigned char)va_arg(arg, unsigned int), 8);
	else if (e->j == 1)
		tmp = ft_ltoabase(va_arg(arg, uintmax_t), 8);
	else if (e->z == 1)
		tmp = ft_ltoabase(va_arg(arg, size_t), 8);
	else
		tmp = ft_ltoabase(va_arg(arg, unsigned int), 8);
	print_flag(tmp, e);
	ft_memdel((void*)&tmp);
}
