/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_m.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:26:16 by npatton           #+#    #+#             */
/*   Updated: 2018/08/12 23:00:13 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		convert_m(t_env *e, va_list arg)
{
	char	*tmp;

	e->type = 'p';
	tmp = ft_ltoabase((long)va_arg(arg, void *), 16);
	print_flag(tmp, e);
	ft_memdel((void*)&tmp);
}
