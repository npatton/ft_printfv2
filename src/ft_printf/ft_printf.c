/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/13 19:13:02 by npatton           #+#    #+#             */
/*   Updated: 2018/10/13 19:13:07 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_vfprintf(int fd, const char *format, va_list arg)
{
	t_env		*e;
	int			ret;

	ret = 0;
	if (!format)
		return (0);
	e = env_alloc(fd);
	env_init(e);
	e->fmt = format;
	while(e->fmt[e->i])
	{
		if (e->fmt[e->i] != '%')
			buf_chr(e->fmt[e->i++], e);
		if (!e->fmt[e->i])
			break;
		parse_fmt(e, arg);
	}
	ret = write(e->fd, e->buf, ft_strlen(e->buf));
	ft_memdel((void *)&e->buf);
	ft_memdel((void *)&e);
	return (ret);
}

int			ft_dprintf(int fd, const char *format, ...)
{
	va_list		arg;
	int			ret;

	va_start(arg, format);
	ret = ft_vfprintf(fd, format, arg);
	va_end(arg);
	return (ret);
}

int			ft_printf(const char *format, ...)
{
	va_list		arg;
	int			ret;

	va_start(arg, format);
	ret = ft_vfprintf(1, format, arg);
	va_end(arg);
	return (ret);
}
