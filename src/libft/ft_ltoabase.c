/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ltoabase.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 13:01:05 by npatton           #+#    #+#             */
/*   Updated: 2018/08/10 11:06:58 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_malloc_base(unsigned int c, size_t base)
{
	size_t	i;

	i = 0;
	while (c > 1)
	{
		c = c / base;
		i++;
	}
	return (i);
}

char		ft_converttochar(int c)
{
	if (c > 9)
		return ('a' + (c - 10));
	else
		return ('0' + c);
}

char		*ft_ltoabase(unsigned long c, size_t base)
{
	int		i;
	char	*ret;

	if (base > 16)
		return (NULL);
	if (!c)
		return (0);
	i = 0;
	ret = (char*)malloc(sizeof(char) * (ft_malloc_base(c, base) + 1));
	while (c != 0)
	{
		ret[i++] = ft_converttochar(c % base);
		c = c / base;
	}
	ret[i] = '\0';
	return (ft_strrev(ret));
}
